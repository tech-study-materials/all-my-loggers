import library_using_jul.JulService
import library_using_log4j.Log4jService
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler

fun main() {
    SLF4JBridgeHandler.removeHandlersForRootLogger()
    SLF4JBridgeHandler.install()
    AppWithBridges().run()
}

class AppWithBridges {

    fun run() {
        JulService().tellMeAboutYourself()
        Log4jService().tellMeAboutYourself()
        LoggerFactory.getLogger(javaClass).info("Hello")
    }
}