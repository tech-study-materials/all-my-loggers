plugins {
    kotlin("jvm") version "1.6.10"
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.slf4j:slf4j-api:1.7.36")
    implementation("ch.qos.logback:logback-classic:1.2.10")

    implementation(project(":library-using-jul"))
    implementation("org.slf4j:jul-to-slf4j:1.7.36")


    implementation(project(":library-using-log4j")) {
        exclude(group = "org.apache.logging.log4j", module = "log4j-core")
    }
    //this is for log4j v.1xxx:
//    implementation("org.slf4j:log4j-over-slf4j:1.7.36")
    //this is for log4j2
    implementation("org.apache.logging.log4j:log4j-to-slf4j:2.17.1")
}
