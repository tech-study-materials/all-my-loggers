rootProject.name = "all-my-loggers"
include("library-using-jul", "library-using-log4j", "app", "app-with-slf4j","app-with-slf4j-and-bridges", "springboot-app")