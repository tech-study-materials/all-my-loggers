package bootapp

import org.slf4j.LoggerFactory
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

fun main() {
    runApplication<BootApp>()
}

@SpringBootApplication
class BootApp :  ApplicationRunner {
    override fun run(args: ApplicationArguments) {
        LoggerFactory.getLogger(javaClass).info("Hello")
    }

}