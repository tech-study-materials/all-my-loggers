package library_using_jul

import java.util.logging.Logger

fun main() {
    JulService().tellMeAboutYourself()
}

class JulService {
    fun tellMeAboutYourself() {
        Logger.getLogger(javaClass.name).info("Hello")
    }
}