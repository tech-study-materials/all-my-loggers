import library_using_jul.JulService
import library_using_log4j.Log4jService
import org.slf4j.LoggerFactory

fun main() {
    AppWithoutBridges().run()
}

class AppWithoutBridges {

    fun run() {
        JulService().tellMeAboutYourself()
        Log4jService().tellMeAboutYourself()
        LoggerFactory.getLogger(javaClass).info("Hello")
    }
}