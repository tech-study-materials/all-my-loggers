import library_using_jul.JulService
import library_using_log4j.Log4jService

fun main() {
    JulService().tellMeAboutYourself()
    Log4jService().tellMeAboutYourself()
}