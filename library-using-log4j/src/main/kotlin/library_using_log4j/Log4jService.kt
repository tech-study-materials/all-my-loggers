package library_using_log4j

import org.apache.logging.log4j.LogManager

fun main() {
    Log4jService().tellMeAboutYourself()
}

class Log4jService {

    fun tellMeAboutYourself() {
        LogManager.getLogger(javaClass).info("Hello")
    }
}